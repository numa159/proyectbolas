extends Spatial

class_name BallFollower

var offset

onready var ball = get_parent().get_node("RigidBody")

func _ready(): # Translation = position en 2D
	offset = translation - ball.translation
	
func _physics_process(delta):
	translation = ball.translation + offset
	
