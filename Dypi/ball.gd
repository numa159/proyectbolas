extends RigidBody

var movement_speed = 0.25

func _process(delta):
	if Input.is_action_pressed("ui_left"):
		apply_central_impulse(Vector3(0, 0,-movement_speed))
	if Input.is_action_pressed("ui_right"):
		apply_central_impulse(Vector3(0, 0,movement_speed))
	if Input.is_action_pressed("ui_up"):
		apply_central_impulse(Vector3(movement_speed,0 ,0))
	if Input.is_action_pressed("ui_down"):
		apply_central_impulse(Vector3(-movement_speed,0 ,0))
	if Input.is_action_just_pressed("ui_saltar"):
		apply_central_impulse(Vector3(0,2,0))
