extends BallFollower


#onready var ball = get_parent().get_node("RigidBody")

func _physics_process(delta):
	var Vel_Direction = ball.linear_velocity.normalized()
	self.rotation_degrees.y = ((Vector2(Vel_Direction.x, -Vel_Direction.z).angle()) / (2*PI)) * 360
	#print(self.rotation_degrees.y)
	#translation = ball.translation + offset

