#!/bin/sh

proyectTag="Bolas"
proyectFolder=$HOME/Projects/proyectbolas

session=$2 
default=Dev

tmux new-session -d -s ${proyectTag}_Sv -c $proyectFolder/Server docker-compose up
tmux new-session -d -s ${proyectTag}_Instance -c $proyectFolder godot ./Game/project.godot
tmux new-session -d -s ${proyectTag}_Dev -c $proyectFolder vim --servername godot

tmux a -t $session > /dev/null 2>&1 || tmux a -t ${proyectTag}_$default
