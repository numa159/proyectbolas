#!/bin/sh
folder=$1
[ -z "$folder" ] && exit 1

toFolder=$(echo $folder | sed 's#Gifs/##')
[ -d "$toFolder" ] || mkdir "$toFolder"

specFile="$toFolder/spec.json"
[ -f "$specFile" ] && exit 0 

files=$(find "$folder"/*.gif)
first="$(echo "$files" | head -n1)"
last="$(echo "$files" | tail -n1)"
echo "$files" | while read -r file; do
	identified=$(identify -format "T:%T D:%Wx%H\n" "$file")
	frames=$(echo "$identified" | cut -d ' ' -f1 | cut -d ':' -f2)
	dims=$(echo "$identified" | cut -d " " -f2 | cut -d ":" -f2 | head -n 1)
	width=$( echo $dims | cut -d "x" -f1)
	heigth=$(echo $dims | cut -d "x" -f2)

	base="${file%.*}"
	to=$(echo ${base}.png | sed 's#Gifs/##')
	gifsicle -w --colors=255 -b -U "$file" 
	montage "$file" -geometry '1x1+0+0<' -tile x1 -alpha On -background "rgba(0, 0, 0, 0.0)" -quality 100 "$to"

	[ "$file" != "$first" ] && spec="$spec,"
	name=$(basename "$base")
	spec="$spec\"$name\": {\"dims\":{\"x\":$width, \"y\":$heigth}, \"frames\":["
	for line in $frames;
	do
		spec="$spec$line,"
	done
	spec="${spec%,}]}"
	[ "$file" == "$last" ] && echo "{$spec}" > "$specFile"
done
