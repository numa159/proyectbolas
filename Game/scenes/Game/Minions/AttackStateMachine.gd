extends StateMachine

var body : CapsuleMinion setget set_body

# Called when the node enters the scene tree for the first time.
func _ready():
	states_map = {
		"hit": $HitState,
		"approach": $ApproachState,
	}

func set_body(new_body):
	body = new_body
	for state in get_children():
		state.body = new_body
