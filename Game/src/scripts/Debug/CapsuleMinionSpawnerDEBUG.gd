extends Node

export(PackedScene) var load_capsule = null
export(float) var waiting_time_spawn = 5
export(bool) var autostart_spawn_timer = true
export(int) var spawn_capsule_quantity = 5

onready var quantity_spawn_capsules_dic : MinionSpawnList = MinionSpawnList.new({"minion":spawn_capsule_quantity})
onready var capsules_holder_dic : MinionSpawnList = MinionSpawnList.new()
onready var capsule_spawn_timer = Timer.new()

func _unhandled_input(event): #Señal del evento de ingreso a base
	if event is InputEventKey and event.pressed and event.scancode == KEY_SPACE:
		$CapsuleSpawnPoint.spawn_capsules(capsules_holder_dic);
		capsules_holder_dic.reset_holder_list(spawn_capsule_quantity)

func _ready():
	
	add_child(capsule_spawn_timer)
	capsule_spawn_timer.connect("timeout", self, "_on_capsule_spawn_timer_timeout")
	capsule_spawn_timer.wait_time = waiting_time_spawn
	capsule_spawn_timer.autostart = autostart_spawn_timer
	capsule_spawn_timer.start()

func _on_capsule_spawn_timer_timeout():
	capsules_holder_dic.add_capsule_spawn(quantity_spawn_capsules_dic.list)

func set_player(player):
	$CapsuleSpawnPoint.set_player(player)
