extends RigidBody

export(float) var speed = 0.3
export(float) var grad_rotspeed = 8
export(float) var push_magnitude = 10

var respawn_pos = null 

func _ready():
	pass 

func _physics_process(delta):
	var velocity = Vector3(0,0,0)
	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
	if Input.is_action_pressed("ui_left"):
		velocity.x += -speed
	if Input.is_action_pressed("ui_up"):
		velocity.z += -speed
	if Input.is_action_pressed("ui_down"):
		velocity.z += speed
	apply_central_impulse(velocity)
	

	$PushMinionArea/CollisionShape.shape.radius = linear_velocity.length() * 0.075 + 1.25 #Esto es para que no te frene cuando vas muy rapido las capsulitas
func _on_PushMinionArea_body_entered(body):
	if body is CapsuleMinion:
		push_away(body)
		body.disable_collision() 

func push_away(body: CapsuleMinion):
	var push_direction = (body.translation - self.translation + Vector3(0,1,0)).normalized() #en Vector3 el radio de la vola
	body.velocity = push_direction * min(linear_velocity.length(), push_magnitude)
	var grid_pos = body.grid_pos
	if grid_pos:
		MinionHiveMind.unoccupy(grid_pos)

func _integrate_forces(state):
	if respawn_pos != null:
		var t = state.get_transform()
		t.origin = respawn_pos
		respawn_pos = null
		state.set_transform(t)
