extends Spatial

func _unhandled_input(event):
	if event is InputEventKey and event.pressed and event.scancode == KEY_SPACE:
		$MinionManager.spawn()

func _ready():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(42069, 1);
	get_tree().network_peer = peer;
	Lobby._add_self();

func _process(_delta):
	if GameData.players_node:
		$MinionManager.initialize($Players.get_child(0), self)
		set_process(false)
