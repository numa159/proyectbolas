tool
extends Spatial

const ARROW = preload("res://scenes/Game/Map/FloorArrow.tscn")

export(int) var pixels = 3 setget set_pixels;
export(float) var offset = .2 setget set_offset;
export(int) var arrow_count = 3 setget set_count;
export(Color) var color = Color.lime setget set_color;
export(float) var impulse_strength = 1;

var pushing :Array;

onready var direction = \
	(to_global(Vector3(1,0,1))-to_global(Vector3.ZERO)).normalized();

func _ready():
	#warning-ignore: return_value_discarded
	$Area.connect("body_entered", self, "on_body_entered");
	#warning-ignore: return_value_discarded
	$Area.connect("body_exited", self, "on_body_exited");
	settle_arrows();

func valid_body(body):
	if not (body.get_parent() is PlayerNode) or not body.is_network_master():
		return false;
	if body is KinematicBody:
		printerr("Error, player should not be KinematicBody!")
		return false
	return true;

func on_body_entered(body):
	if not valid_body(body): return;
	pushing.append(body)

func on_body_exited(body):
	if not valid_body(body): return;
	if not pushing.has(body):
		printerr("Body not found!");
		return;
	pushing.remove(pushing.find(body));

func _physics_process(_delta):
	for body in pushing:
		body.apply_central_impulse(direction*impulse_strength);

func settle_arrows():
	if not has_node("Arrows"): return;
	for instance in get_node("Arrows").get_children():
		instance.queue_free()
	$Area/CollisionShape.shape.extents.z = sqrt(2*arrow_count*arrow_count);
	for i in arrow_count:
		var inst = ARROW.instance();
		get_node("Arrows").add_child(inst);
		inst.name = "Arrow%d" % i;
		var mat = inst.get_surface_material(0);
		if not mat:
			printerr("No material found!");
		mat.set_shader_param("color", color);
		mat.set_shader_param("offset", -offset * i);
		mat.set_shader_param("pixels", pixels);
		var p = 2*i - (arrow_count-1);
		inst.translation = Vector3(p, 0, p);
	
func set_count(v: int)->void:
	arrow_count = v;
	settle_arrows();

func set_color(v:Color)->void:
	color = v;
	settle_arrows();

func set_pixels(v:int)->void:
	pixels = v;
	settle_arrows();

func set_offset(v:float)->void:
	offset = v;
	settle_arrows();
