tool
extends Spatial

var player : PlayerNode

func _ready():
	$CornerMarker.translation = \
		Vector3(.5,0,.5) * Data.MAP.radius + \
		Vector3.UP * $CornerMarker.translation;
