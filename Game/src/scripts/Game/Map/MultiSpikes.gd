tool
extends Spatial

const POINTS_PATH = "res://assets/resources/points/PitPoints.tres";
var poisson := preload(POINTS_PATH);

var map_side = Data.MAP.radius;
var depth = Data.MAP.pit_depth;
var span = Data.MAP.pit_span
var circle_radius = 2
var spike_transform_list = []
var circle_transform_list = []
var identity = Transform.IDENTITY

const SPIKE = preload("res://assets/resources/models/Spike.tres")
const CIRCLE = preload("res://assets/resources/models/CircleSpike.tres")

func _ready():
	if not poisson.points:
		poisson.generate(
			Vector2(map_side*2,0) + Vector2(1,1)*(span-circle_radius),
			circle_radius 
		);
		#warning-ignore: return_value_discarded
		ResourceSaver.save(POINTS_PATH, poisson);
	for flat_point in poisson.points:
		var transform = \
			identity.translated(Vector3(flat_point.x,0,flat_point.y));
		spike_transform_list.append(transform)
		circle_transform_list.append(transform.translated(Vector3(0,-1.324,0)));

	$Spikes.translation = Vector3(0,-depth,0);
	$Circles.translation = Vector3(0,-depth,0);
	set_multimesh($Spikes,SPIKE,spike_transform_list);
	set_multimesh($Circles,CIRCLE,circle_transform_list);

func set_multimesh(multimesh_instance,mesh,transform_list):
	var multimesh = MultiMesh.new()
	
	multimesh.transform_format = MultiMesh.TRANSFORM_3D
	multimesh.color_format = MultiMesh.COLOR_NONE
	multimesh.custom_data_format = MultiMesh.CUSTOM_DATA_NONE
	
	multimesh.instance_count = len(transform_list)
	multimesh.visible_instance_count = -1;

	for i in multimesh.instance_count:
		multimesh.set_instance_transform(i, transform_list[i])
	
	multimesh.mesh = mesh
	multimesh.set_local_to_scene(true)
	multimesh_instance.multimesh = multimesh
