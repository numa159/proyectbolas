tool
extends MeshInstance

onready var shader_texture := ImageTexture.new();
onready var data_img := Image.new();

func _ready():
	set_physics_process(true);
	mesh.size = Vector2(1,1) * Data.MAP.radius;
	data_img.create(Connection.MAX_PLAYERS,2,false,Image.FORMAT_RGBF);
	shader_texture.create_from_image(data_img,0);
	var mat = get_surface_material(0);
	mat.set_shader_param("data", shader_texture);

func _physics_process(_delta):
	if not GameData.players_node: return;
	data_img.lock();
	var i = 0;
	for player in GameData.players_node.get_children():
		var pos = player.body.global_transform.origin;
		data_img.set_pixel(i,0, Color(pos.x, pos.z, 0.0));
		data_img.set_pixel(i,1, player.color);
		i+=1;
	data_img.unlock();
	shader_texture.set_data(data_img);
