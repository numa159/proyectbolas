tool
extends MeshInstance
class_name Billboard

const PIXEL_SIZE = 1.0/8.0;

export(StreamTexture) var animation_image setget set_anim;
export(int) var animation_frames := 1 setget set_anim_frames;
export(float) var time_mult := 1.0;
export(Color) var led_color := Color.red setget set_color;
var anim_idx = 0;

var texture := TextureArray.new();
var t := 0.0;

func _ready():
	texture.set_local_to_scene(true);
	set_process(true);

func set_color(col:Color):
	led_color = col;
	var mat = get_surface_material(0);
	mat.set_shader_param("color", led_color);

func set_anim(tex: Texture):
	tex.set_local_to_scene(true);
	animation_image = tex;
	create_texture_array();

func set_anim_frames(frames: int):
	animation_frames = frames;
	if frames > 1: set_process(true);
	else: set_process(false);
	create_texture_array();

func create_texture_array():
	if not animation_image: 
		printerr("animation_image needs to be set!")
		return;
	if not animation_frames: 
		printerr("animation_frames cannot be zero!")
		return;
	if not animation_image.get_data(): 
		printerr("animation_image is empty!")
		return;

	var full_size = animation_image.get_size();
	var frame_size = \
		Vector2(floor(full_size.x / animation_frames), full_size.y);

	self.mesh.size = frame_size * PIXEL_SIZE;

	var full_img = animation_image.get_data();
	var images = [];
	for i in range(animation_frames):
		images.push_front(
			full_img.get_rect(
				Rect2(Vector2(frame_size.x * i,0), frame_size)
			)
		)
	texture.create(
		frame_size.x,
		frame_size.y,
		animation_frames,
		full_img.get_format(),
		0
	);
	var i = 0;
	for image in images:
		texture.set_layer_data(image,i);
		i+=1;
	var mat = get_surface_material(0);
	mat.set_shader_param("image", texture);
	mat.set_shader_param("frame_size", frame_size);

func _process(delta):
	t += delta * time_mult;
	if t > animation_frames:
		t = 0;
	var int_t = int(floor(t)) % animation_frames
	if int_t < 0:
		int_t = animation_frames+int_t;
	if int_t != anim_idx:
		var mat = get_surface_material(0);
		mat.set_shader_param("idx", int_t % animation_frames);
		anim_idx = int_t;
