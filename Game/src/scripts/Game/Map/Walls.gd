tool
extends Spatial

const WALL_DEPTH = 5;

func _ready():
	var wall = get_node("xWall");
	wall.translation = Vector3(
		Data.MAP.radius*.5 + Data.MAP.pit_span,
		-Data.MAP.wall_height, 
		Data.MAP.radius*.5 + Data.MAP.pit_span
	);
	wall.rotation = Vector3.ZERO;
	wall.scale.y = Data.MAP.wall_height * 2;
	wall.scale.x = -(Data.MAP.radius + Data.MAP.pit_span)*2;
	wall.scale.z = WALL_DEPTH;
