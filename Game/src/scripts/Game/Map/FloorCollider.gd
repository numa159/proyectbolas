tool
extends StaticBody

func _ready():
	set_meta("is_floor", true);
	var half_depth = Data.MAP.pit_depth / 2.0;
	$CollisionShape.translation.y =  - half_depth;
	$CollisionShape.shape.extents = \
		Vector3(1,0,1) * Data.MAP.radius + Vector3.UP * half_depth;
