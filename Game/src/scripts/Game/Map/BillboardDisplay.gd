tool
extends Spatial

const BILLBOARD = preload("res://scenes/Game/Map/Billboard.tscn");

export(StreamTexture) var animation_image;
export(int) var animation_frames := 1;
export(float) var time_mult := 1.0;
export(Color) var led_color := Color.red;

func _ready():
	for child in get_children():
		child.queue_free();
	var bill =  BILLBOARD.instance();
	bill.time_mult = time_mult;
	bill.animation_frames = animation_frames;
	bill.led_color = led_color;
	bill.animation_image = animation_image;
	add_child(bill);
