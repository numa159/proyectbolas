tool
extends Spatial

export(PackedScene) var quadrant = \
	preload("res://scenes/Game/Map/Quadrant.tscn");

func _ready():
	set_process(true)
	$MapContents/CameraFloor.translation.y = Data.CAMERA.height - .25;
	$MapContents/DespawnArea.translation.y = -Data.MAP.pit_depth;
	for i in range(4):
		var q = quadrant.instance()
		var xv = floor(i/2.0);
		var yv = (i%2);
		q.translation = Vector3(1,0,0)*(xv*2-1)*Data.MAP.radius*.5 + \
				Vector3(0,0,1)*(yv*2-1)*Data.MAP.radius*.5;
		q.name = "Quad:%d,%d" % [xv,yv];
		# q.rotation_degrees.y = 0;
		q.rotation_degrees.y = 180*(int(xv+yv+1)%2) +sign(xv*2-1)*90*(yv+xv);
		$Quadrants.add_child(q);

func _process(_delta):
	if GameData.players_node:
		#warning-ignore: return_value_discarded
		GameData.players_node.connect(
			"all_players_ready",
			self,
			"_on_players_ready"
		);
		set_process(false)

func _on_players_ready():
	for i in GameData.players_node.get_children().size():
		var quad = $Quadrants.get_child(i)
		var player = GameData.players_node.get_child(i)
		quad.player = player
		quad.get_node("MinionManager").initialize(player,self)
