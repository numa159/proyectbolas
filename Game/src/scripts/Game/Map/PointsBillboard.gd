tool
extends Spatial

const LABEL = preload("res://assets/textures/DisplayAnimations/Score.png");
const BILLBOARD = preload("res://scenes/Game/Map/Billboard.tscn");
const MAX_DIGITS = 5;
const MAX_POINTS = 10000;

var initialized := false;
var renderer := NumberRenderer.new();

func initialize():
	if not GameData.players_node: return;
	#warning-ignore: return_value_discarded
	GameData.players_node.connect("all_players_ready", self, "initial_setup");
	set_process(false)

func initial_setup():
	initialized = true;
	var i := 0;
	for player in GameData.players_node.get_children():
		var bill =  BILLBOARD.instance();
		bill.name = "%d"%i;
		bill.animation_frames = 1;
		bill.led_color = player.color;
		bill.translation.z = (0.5 + i) * NumberRenderer.DIGIT_SIZE * \
			Billboard.PIXEL_SIZE;
		add_child(bill);
		i+=1;
	create_label();
	#warning-ignore: return_value_discarded
	GameData.connect("points_updated", self, "show_points");
	show_points();

func _ready():
	for child in get_children():
		child.queue_free();
	set_process(true);

func _process(_delta):
	initialize();

func create_label():
	var bill =  BILLBOARD.instance();
	bill.name = "Label"
	bill.animation_frames = 1;
	bill.led_color = Color.white;
	bill.translation.z = -0.5 * NumberRenderer.DIGIT_SIZE * \
		Billboard.PIXEL_SIZE;
	bill.animation_image = LABEL;
	add_child(bill);

func show_points():
	for i in range(Lobby.player_count):
		var points = GameData.points[i];
		var b = get_node(String(i));
		b.animation_image = set_points(points);

func set_points(points: int)->ImageTexture:
	var image_tex = ImageTexture.new();
	image_tex.create_from_image(
		renderer.create_number_image(int(min(points,MAX_POINTS)), MAX_DIGITS),
		0
	);
	return image_tex;
