extends Area

export(int) var depth_despawn = 10

onready var point_respawn = Vector3(0,3,0)

var time_respawn = 5

func _ready():
	self.translation.y = get_parent().translation.y - depth_despawn

func _on_DespawnArea_body_entered(body_entered):
	if body_entered is RigidBody:
		body_entered.get_parent().visible = false
		body_entered.get_parent().dead = true
		yield(get_tree().create_timer(time_respawn), "timeout")
		body_entered.get_parent().respawn_pos = point_respawn
		body_entered.linear_velocity = Vector3(0,0,0)
		body_entered.set_angular_velocity(Vector3(0,0,0))
		body_entered.get_parent().visible = true
		body_entered.get_parent().dead = false
	if body_entered is CapsuleMinion:
		body_entered.queue_free()
