extends Spatial

export(int) var number := 0 setget set_number;
export(Color) var color := Color.white setget set_color;
export(float) var sprite_scale := 1.0 setget set_sprite_scale;

var renderer := NumberRenderer.new();
var image := ImageTexture.new();

func _ready():
	if not number and not Engine.editor_hint:
		printerr("No points!");
		queue_free();

func set_sprite_scale(v:float):
	sprite_scale = v;
	get_child(0).scale = Vector3(1,1,1) * v;

func set_number(v :int):
	number = v;
	var digits_num = ceil(log(v+1) / log(10));
	var num = renderer.create_number_image(v, digits_num);

	var plus = Image.new();
	plus.create(3,3,false,num.get_format());
	plus.fill(Color.black);
	plus.lock();
	for i in range(3):
		plus.set_pixel(1,i, Color.white);
		plus.set_pixel(i,1, Color.white);
	plus.unlock();

	var size = num.get_size();
	size += Vector2(plus.get_size().x+1, 0);

	var final = Image.new();
	final.create(size.x, size.y, num.get_format(),0);
	final.lock();
	final.blit_rect(
		num, Rect2(Vector2.ZERO, num.get_size()), Vector2(4,0)
	);
	final.blit_rect(
		plus, Rect2(Vector2.ZERO, plus.get_size()), Vector2(1,2)
	);
	final.unlock();

	get_child(0).mesh.size = final.get_size();
	var mat = get_child(0).get_surface_material(0);
	mat.set_shader_param("texture", image);

	image.create_from_image(final, 0);

func set_color(v: Color):
	color = v;
	var mat = get_child(0).get_surface_material(0);
	mat.set_shader_param("color", color);
