extends Spatial

export(float) var alive_time := 1;
export(int) var points_value := 5;
export(Color) var color := Color.red;

const POINTS = preload("res://scenes/Game/Points.tscn");

func _ready():
	var node = POINTS.instance();
	node.number = points_value;
	node.color = color;

	#[NOTE](@dogor): 
	#	Números mágicos para q el numero sea más grande 
	#	cuanto mas grande el numero.
	node.sprite_scale = 0.75 * pow(1.0005, points_value);
	add_child(node);

	var t = Tween.new();
	add_child(t)
	t.interpolate_property(node, "color:a", null, 0, alive_time);
	t.interpolate_property(node, "translation:y", null, 5, alive_time);
	t.start();
	yield(t, 'tween_all_completed');
	node.queue_free();
	t.queue_free();
	queue_free();
