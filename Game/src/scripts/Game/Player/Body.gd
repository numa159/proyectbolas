extends RigidBody

func _integrate_forces(state):
	var respawn_pos = get_parent().respawn_pos;
	if respawn_pos != null:
		get_node("Trail3D").clear_points();
		var t = state.get_transform();
		t.origin = respawn_pos;
		get_parent().respawn_pos = null;
		state.set_transform(t);
