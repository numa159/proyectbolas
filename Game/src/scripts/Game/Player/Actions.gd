extends Spatial

onready var ball = get_parent().get_node("MeshInstance");
onready var shield = get_parent().get_node("Shield");
var block_tween = Tween.new();

func _ready():
	add_child(block_tween);

func attack(event:InputEvent)->void:
	if event.is_pressed():
		Debug.print("Attack charging")
	else:
		Debug.print("Attack released")

func block(event: InputEvent)->void:
	if event.is_pressed():
		Debug.print("Blocking pressed")
		block_tween.interpolate_method(
			self,
			"update_shield",
			0,
			1, 
			0.25,
			Tween.TRANS_ELASTIC, 
			Tween.EASE_IN
		)
	else:
		Debug.print("Blocking released")
		block_tween.interpolate_method(
			self,
			"update_shield",
			1, 
			0,
			1.0,
			Tween.TRANS_LINEAR, 
			Tween.EASE_OUT
		)
	block_tween.start();

func update_shield(t:float):
	print("updating shield")
	var shield_mat = shield.get_surface_material(0)
	var col = shield_mat.get_shader_param("color");
	col.a = t;
	shield_mat.set_shader_param("color", col);
	var ball_mat = ball.get_surface_material(0)
	ball_mat.emission_energy = 10 - t * 10;

func _unhandled_input(event:InputEvent)->void:
	if event.is_action("block"):
		block(event);
	if event.is_action("attack"):
		attack(event);
