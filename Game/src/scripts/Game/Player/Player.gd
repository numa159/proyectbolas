extends Spatial
class_name PlayerNode

export(float) var speed = .5;
export(Color) var color = Color.white setget set_color;
export(float) var push_magnitude = 10
export (Resource) var health

const AIRBORNE_CONTROL_T = 3.0

var body : PhysicsBody;
var body_mesh : MeshInstance;
var camera_collision:KinematicBody;
var on_floor := false setget set_on_floor;
var controllable := false;

var changes := 0;
var acc_update_delay := 0.0;
var avg_update_delay := 0.05;
var last_time := 0.0;
var last_transform: Transform;
sync var body_transform: Transform setget set_transform;
sync var vel:Vector3 = Vector3.ZERO;
var id := 0;
var t = 0.0;
var dead = false

var respawn_pos = null;

func _ready():
	if not is_network_master():
		#Remove camera collision and true body from every other player
		$Body.queue_free();
		$CameraRig.queue_free();

		#Set mock body as body
		body = $MockBody;
		body.name = "Body";
	else:
		#Remove mock body from player
		body = $Body;
		$MockBody.queue_free();
		$CameraRig.set_follow_player(self);
		#warning-ignore: return_value_discarded
		body.connect("body_entered", self, "on_start_colliding");
		#warning-ignore: return_value_discarded
		body.connect("body_exited", self, "on_stop_colliding");

	#warning-ignore: return_value_discarded
	$BodyCharas/PushArea.connect(
		'body_entered', 
		self, 
		"on_body_entered"
	);

	#Reparent body parts to body.
	var body_nodes = $BodyCharas.get_children();
	for node in body_nodes:
		$BodyCharas.remove_child(node)
		body.add_child(node);
	#Fix reparenting
	body.get_node("Trail3D")._target = body;
	body.get_node("Trail3D").global_transform.origin = \
		body.global_transform.origin * Vector3(1,0,1)+Vector3(0,0.15,0);

	#Cleanup
	$BodyCharas.queue_free();
	body_mesh = body.get_node("MeshInstance");


func set_color(col:Color):
	var mat = body.get_node("MeshInstance").get_surface_material(0);
	mat.emission_enabled = true;
	mat.emission = col;
	var trail_mat = body.get_node("Trail3D").material_override;
	trail_mat.set_shader_param("color", col);
	color = col;
	col.a = 0;
	body.get_node("Shield")\
		.get_surface_material(0)\
		.set_shader_param("color", col);

func set_transform(tr:Transform):
	last_transform = body_transform;
	acc_update_delay += t - last_time;
	last_time = t;
	changes += 1;
	avg_update_delay = acc_update_delay / changes;
	body_transform = tr;

func _physics_process(delta):
	t += delta;
	if is_network_master():
		body.apply_central_impulse(compute_frame_impulse());
		rset_unreliable("body_transform", body.global_transform);
		rset_unreliable("vel", body.linear_velocity);
	elif body_transform and vel and last_transform:
		var percent = \
			(t-last_time) /max(avg_update_delay,0.01) - \
			floor((t-last_time)/max(avg_update_delay,0.01));
		body.global_transform = \
			last_transform.interpolate_with(body_transform, percent);
		body.move_and_slide(vel,Vector3.UP);

func compute_frame_impulse():
	var instant_impulse = Vector3.ZERO;
	if not OS.is_window_focused() or not controllable: 
		return instant_impulse;

	if Input.is_action_pressed("ui_right"):
		instant_impulse.x += speed;
	if Input.is_action_pressed("ui_left"):
		instant_impulse.x += -speed;
	if Input.is_action_pressed("ui_up"):
		instant_impulse.z += -speed;
	if Input.is_action_pressed("ui_down"):
		instant_impulse.z += speed;	
	
	if body_transform:
		var pos = body_transform.origin;
		var dir = (pos-$CameraRig.global_transform.origin).normalized();
		var angle = Vector2(-dir.z, -dir.x).angle();
		instant_impulse = instant_impulse.rotated(Vector3(0,1,0),angle);
	body.get_node("PushArea/CollisionShape").shape.radius = \
		vel.length() * 0.075 + 1.25 
		#[NOTE](@lordmanuel): Números mágicos para el crecimiento del area.
	return instant_impulse* Vector3(1,0,1);

func on_body_entered(entered_body):
	if entered_body.has_meta("pushable"):
		push_away(entered_body)
		
func hurt(amount):
	health.take_damage(amount)
	
func push_away(b_to_push):
	b_to_push.disable_collision() 
	var push_direction = (
		b_to_push.translation - 
		body.global_transform.origin + 
		Vector3.UP * Data.BALL.radius 
	).normalized() 
	b_to_push.velocity = \
		push_direction * min(vel.length(), push_magnitude)
	var grid_pos = b_to_push.grid_pos
	if grid_pos:
		MinionHiveMind.unoccupy(grid_pos)

func set_layer():
	body.set_collision_layer_bit((id*2)+1,true)

func on_stop_colliding(with):
	if with.has_meta("is_floor"):
		set_on_floor(false);

func on_start_colliding(with):
	if with.has_meta("is_floor"):
		set_on_floor(true);

func update_control():
	controllable = on_floor;

func set_on_floor(v:bool):
	on_floor = v;
	if on_floor:
		update_control();
	else:
		var timer = get_tree().create_timer(AIRBORNE_CONTROL_T);
		timer.connect("timeout", self, "update_control");
