tool
extends KinematicBody

var following_player: PlayerNode;
var max_camera_accel:int = Data.CAMERA.acceleration;
var min_camera_dist :int = Data.CAMERA.distance;

func _ready():
	global_transform.origin.y = Data.CAMERA.height + 2.5;
	global_transform.origin.z = -Data.MAP.radius;
	$Camera.far = sqrt(2) * 2 * (Data.MAP.radius+Data.MAP.pit_span);

func set_follow_player(player:PlayerNode):
	following_player = player;

func _physics_process(_delta):
	if not following_player:
		return;
	if not $Camera.current:
		$Camera.current = true;
	var player_pos = following_player.body_transform.origin;
	$Camera.look_at(player_pos, Vector3.UP);
	var to_player = (Vector3(1,0,1)*(player_pos - self.translation));
	var wanted_pos = \
		player_pos*Vector3(1,0,1) + \
		(-1 * to_player.normalized() * min_camera_dist);
	var to_wanted = (Vector3(1,0,1)*(wanted_pos - self.translation));
	var dist = to_wanted.length();
	var accel = min(dist, max_camera_accel)
	#warning-ignore: return_value_discarded
	move_and_slide(to_wanted.normalized() * accel, Vector3.UP)
