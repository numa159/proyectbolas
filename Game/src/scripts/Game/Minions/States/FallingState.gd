extends State

onready var FallingJumpTween = Tween.new()

var body : CapsuleMinion

func _ready():
	add_child(FallingJumpTween)

func enter():
	fall_jump()

func exit():
	FallingJumpTween.remove_all()

func update(_delta):
	if body.floored:
		emit_signal("finished","recovering")

func fall_jump():
	var main_animations = {
		"rotation_degrees:x":-20,
		"rotation_degrees:z":0,
	};

	TweenAnimator.animate(FallingJumpTween, body.jump_time/2, main_animations, body.mesh);
	for hand in ["RHand", "LHand"]:
		var node = body.get_node(hand);
		var anim = {"translation:z":0.72};
		TweenAnimator.animate(FallingJumpTween, body.jump_time/2, anim, node);
	FallingJumpTween.start()
