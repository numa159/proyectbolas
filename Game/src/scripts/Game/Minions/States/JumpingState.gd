extends State

onready var animation_tweens = {
	PreparationJumpTween = Tween.new(),
	InitialJumpTween = Tween.new(),
}

var body : CapsuleMinion

var target_point : Vector3
var grid_pos = null

func _ready():
	for key in animation_tweens.keys():
		add_child(animation_tweens[key]);

func enter():
	if not body.target_point:
		emit_signal("finished","actioning")
		return
	body.velocity = Vector3.ZERO;
	target_point = body.target_point
	jump()
	
func exit():
	for key in animation_tweens.keys():
		animation_tweens[key].remove_all()

func update(_delta):
	if not body.jumping and body.floored:
		emit_signal("finished", "actioning");

func jump():
	body.jumping = true;
	yield(prepare_jump(), "completed")
	
	if body.grid_pos:
		MinionHiveMind.unoccupy(body.grid_pos)
	body.grid_pos = (MinionHiveMind.map_point_to_grid(target_point))
	body.velocity = generate_velocity_to_point(body.jump_time, target_point)
	
	init_jump()
	MinionHiveMind.occupy(body.grid_pos, body)
	body.jumping = false;

func prepare_jump():
	var PreparationJumpTween = animation_tweens["PreparationJumpTween"]

	var main_animations = {
		"rotation_degrees:z":0,
		"rotation_degrees:x":20,
		"scale:y": 1*0.8,
	};
	TweenAnimator.animate(PreparationJumpTween, body.jump_delay/2, main_animations, body.mesh);
	for hand in ["RHand", "LHand"]:
		var node = body.get_node(hand);
		var anim = {"translation:z":-0.1};
		TweenAnimator.animate(PreparationJumpTween, body.jump_delay/2, anim, node);
	PreparationJumpTween.start()
	yield(PreparationJumpTween, "tween_all_completed")

func init_jump():
	var InitialJumpTween = animation_tweens["InitialJumpTween"]

	var main_animations = {
		"rotation_degrees:x":0,
		"scale:y": 1,
	};
	TweenAnimator.animate(InitialJumpTween, body.jump_time/2, main_animations, body.mesh);
	for hand in ["RHand", "LHand"]:
		var node = body.get_node(hand);
		var anim = {"translation:z":0.24};
		TweenAnimator.animate(InitialJumpTween, body.jump_time/2, anim, node);
	InitialJumpTween.start()

func generate_velocity_to_point(time_of_jump:float, target:Vector3):
	var dir = (target - body.origin).normalized()
	var dist = (target - body.origin).length()
	var result = (dist / time_of_jump) * dir
	result.y = (target.y - body.origin.y) / time_of_jump + (9.8/2) * time_of_jump
	return result
