extends State

onready var RecoveryJumpTween = Tween.new()

var body : CapsuleMinion

func _ready():
	add_child(RecoveryJumpTween)

func enter():
	body.velocity = Vector3.ZERO;
	recover_jump()

func exit():
	RecoveryJumpTween.remove_all()

func recover_jump():
	var main_animations = {
		"rotation_degrees:x":0,
	};

	TweenAnimator.animate(RecoveryJumpTween, body.jump_delay/2, main_animations, body.mesh);
	for hand in ["RHand", "LHand"]:
		var node = body.get_node(hand);
		var anim = {"translation:z":0.24};
		TweenAnimator.animate(RecoveryJumpTween, body.jump_delay/2, anim, node);
	TweenAnimator.animate(RecoveryJumpTween, body.jump_delay/4, {"scale:y":1*0.8}, body.mesh);
	RecoveryJumpTween.start()
	yield(RecoveryJumpTween, "tween_completed")
	TweenAnimator.animate(RecoveryJumpTween, body.jump_delay/4, {"scale:y":1}, body.mesh);
	yield(RecoveryJumpTween, "tween_all_completed")
	emit_signal("finished","actioning")
