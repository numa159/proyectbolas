extends State

var body : CapsuleMinion setget set_body

var animating = false

#warning-ignore: unused_argument
func update(delta):
	if animating:
		return
	animating = true
	print("ENTRANDO A LOOKING")
	yield($LookingAnimation.start_looking_animation(),"completed")
	print("TERMINANDO LA ANIMACION")
	var randsign_x = (randi()%2)*2-2
	var randsign_z = (randi()%2)*2-2 #[TODO] RANDOMIZAR LAS COSAS ESTAN TENDIENDO A UN LUGAR
	var random_dir = Vector3(randi()*randsign_x,0,randi()*randsign_z).normalized()
	var target_point = random_dir * body.jump_distance + body.translation
	body.target_point = MinionHiveMind.get_nearest(target_point)
	if target_point != null:
		animating = false
		get_parent().get_parent().emit_signal("finished", "jumping")

func set_body(new_body):
	body = new_body
	$LookingAnimation.body = new_body
