extends Node

var body : CapsuleMinion

var IdlingTween : Tween
var CleanupTween : Tween
var duration = 2.2 * 2
var idling = false

const TRANS = Tween.TRANS_CIRC
const EASE = Tween.EASE_IN_OUT

func start_idling_animation():
	IdlingTween = Tween.new()
	add_child(IdlingTween)
	
	idling = true
	while idling:
		yield(idling_animation(), "completed")
		pass

func stop_idling():
	if not idling:
		return;
	idling = false
	if not IdlingTween.remove_all():
		printerr("Error removing tween");
	IdlingTween.queue_free();

func animate_step(step:Vector2, step_count: int, tween: Tween):
	if not idling or not tween: return;
	var vector_map = {"x":"scale:y", "y":"rotation_degrees:z"};
	for key in vector_map.keys():
		if not tween.interpolate_property(
			body.mesh, 
			vector_map[key], 
			null, 
			step[key],
			duration/step_count, 
			TRANS, 
			EASE
		):
			 printerr("Error interpolating property on animation step.");
	if not tween.start():
		printerr("Error starting tween");


func idling_animation():
	var default_state = Vector2(1, 0);

	var steps = [
		default_state * Vector2(0.8,1) + Vector2(0,15),
		default_state * Vector2(0.8,1) + Vector2(0,-15),
	];

	for step in steps:
		if not idling or not IdlingTween: return;
		animate_step(step, steps.size()*2, IdlingTween);
		yield(IdlingTween, "tween_all_completed")
		animate_step(default_state, steps.size()*2, IdlingTween);
		yield(IdlingTween, "tween_all_completed")
		yield(get_tree().create_timer(randf()*.5), "timeout")
