extends "res://src/classes/StateMachine.gd"

var body : CapsuleMinion setget set_body
var target_not_found = false

func _ready():
	states_map = {
		"attack": $AttackState,
		"follow": $FollowState,
		"looking": $LookingState,
		"idle" : $IdleState,
	}

func set_body(new_body):
	body = new_body
	for state in get_children():
		state.body = new_body

func _change_state(state_name):
	Debug.print(state_name)
	._change_state(state_name)
