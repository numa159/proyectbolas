extends Node

var LookingTween : Tween
var body : CapsuleMinion

const TRANS = Tween.TRANS_LINEAR
const EASE = Tween.EASE_IN_OUT
const DURATION = 2

func start_looking_animation():
	LookingTween = Tween.new()
	add_child(LookingTween)
	
	yield(looking_animation(), "completed")

func looking_animation():
	print("COMENZANDO LOOKING")
	LookingTween.interpolate_property(body.mesh,"rotation_degrees:y",null,30,DURATION/4, 
		TRANS, 
		EASE
	)
	LookingTween.interpolate_property(body.mesh,"rotation_degrees:z",null,-20,DURATION/4, 
		TRANS, 
		EASE
	)
	LookingTween.start()
	yield(LookingTween,"tween_completed")
	LookingTween.interpolate_property(body.mesh,"rotation_degrees:y",null,-30,DURATION/2, 
		TRANS, 
		EASE
	)
	LookingTween.interpolate_property(body.mesh,"rotation_degrees:z",null,20,DURATION/2, 
		TRANS, 
		EASE
	)
	yield(LookingTween,"tween_completed")
	LookingTween.interpolate_property(body.mesh,"rotation_degrees:y",null,0,DURATION/4, 
		TRANS, 
		EASE
	)
	LookingTween.interpolate_property(body.mesh,"rotation_degrees:z",null,0,DURATION/4, 
		TRANS, 
		EASE
	)
	yield(LookingTween,"tween_all_completed")
