extends State

onready var animation_node = $IdlingAnimation

var body : CapsuleMinion setget set_body

func enter():
	body.velocity = Vector3.ZERO
	if not get_parent().target_not_found:
		print("Idling animation")
		animation_node.start_idling_animation()
	if check_exit_condition():
		return

func exit():
	if get_parent().target_not_found:
		return
	animation_node.stop_idling()

func update(_delta): #NOTA EL ATAQUE TIENE QUE PODER PASAR A FOLLOWING SI PLAYER SE ALEJA
	check_exit_condition()
	
func check_exit_condition():
	var player_dead = body.owned_by.dead
	var dist_player = INF if player_dead else body.to_player.length()
	
	if (MinionHiveMind.get_nearest_enemy(body) != null or \
		len(body.get_node("AggroArea").get_overlapping_bodies()) > 0) and \
		dist_player <= Data.MINION.player_range:
		emit_signal("finished","attack")
		return true
	
	if dist_player >= 8*Data.BALL.radius and not player_dead: 
		emit_signal("finished","follow")
		return true
	
	if player_dead:
		emit_signal("finished","looking")
		return true
	
	return false

func set_body(new_body):
	body = new_body
	$IdlingAnimation.body = new_body
