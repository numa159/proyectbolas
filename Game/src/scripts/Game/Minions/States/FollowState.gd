extends State

var body : CapsuleMinion

func enter():
	var random_dir = \
		Vector3(randi(),0,randi()).normalized()*Data.BALL.radius;
	var to_target = body.to_player + random_dir
	var target_point = \
		to_target.normalized() * min(to_target.length(), body.jump_distance) + \
		body.translation

	body.target_point = MinionHiveMind.get_nearest(target_point)

	if target_point != null:
		get_parent().target_not_found = false
		get_parent().get_node("IdleState").animation_node.stop_idling()
		get_parent().get_parent().emit_signal("finished", "jumping")
	else:
		get_parent().target_not_found = true
		emit_signal("finished", "idling")

func exit():
	return

#warning-ignore: unused_argument
func handle_input(event):
	return

#warning-ignore: unused_argument
func update(delta):
	return
