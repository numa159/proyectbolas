extends State

var body : CapsuleMinion setget set_body

func enter():
	$AttackStateMachine.set_active(true)

func exit():
	$AttackStateMachine.set_active(false)

func set_body(new_body):
	body = new_body
	$AttackStateMachine.body = new_body
