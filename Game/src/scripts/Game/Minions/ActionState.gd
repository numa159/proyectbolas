extends State

var body : CapsuleMinion setget set_body

func enter():
	$ActionStateMachine.set_active(true)

func exit():
	$ActionStateMachine.set_active(false)
	
func set_body(new_body):
	body = new_body
	$ActionStateMachine.body = body
