extends Spatial

export(PackedScene) var load_capsule = null
export(float) var waiting_time_spawn = 5
export(bool) var autostart_spawn_timer = true
export(int) var spawn_capsule_quantity = 5

onready var quantity_spawn_capsules_dic : MinionSpawnList = \
	MinionSpawnList.new({"minion":spawn_capsule_quantity})
onready var capsules_holder_dic : MinionSpawnList = \
	MinionSpawnList.new({"minion":0,"ranged_minion":0,"mage_minion":1}) 
	#[TODO]: Este todo en 0 para la producción
onready var capsule_spawn_timer = Timer.new()

func spawn(body): #Señal del evento de ingreso a base
	if not get_parent().player or body != get_parent().player.body:
		return
	$CapsuleSpawnPoint.spawn_capsules(capsules_holder_dic);
	capsules_holder_dic = MinionSpawnList.new()

func _ready():
	#warning-ignore: return_value_discarded
	$TriggerSpawnArea.connect("body_entered",self,"spawn")
	
	add_child(capsule_spawn_timer)
	capsule_spawn_timer.connect(
		"timeout", 
		self, 
		"_on_capsule_spawn_timer_timeout"
	);
	capsule_spawn_timer.wait_time = waiting_time_spawn
	capsule_spawn_timer.autostart = autostart_spawn_timer;
	capsule_spawn_timer.start()

func _on_capsule_spawn_timer_timeout():
	capsules_holder_dic.add_capsule_spawn(quantity_spawn_capsules_dic.list)

func initialize(player, capsule_parent):
	$CapsuleSpawnPoint.set_player(player)
	$CapsuleSpawnPoint.set_capsule_parent(capsule_parent)
