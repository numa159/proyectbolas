extends RangedProjectile

onready var _animator = $AnimationPlayer

func _init():
	speed = 10.0
	time_in_flight = 3.0

func _on_body_entered(body):
	if body.name == "CapsuleMinionBody": #[TODO] Sacar cuando tengamos sistema de layers
		return
	._on_body_entered(body)
	_animator.play("Hit")
