extends Node
class_name RangedProjectile

var speed : float
var time_in_flight : float
var direction := Vector3(0,0,1)

onready var kill_timer = get_tree().create_timer(time_in_flight)

signal hitted_something(body)

func _ready():
	#warning-ignore: return_value_discarded
	$HitArea.connect("body_entered", self, "_on_body_entered")
	direction = direction.rotated(Vector3.UP, self.rotation.y)
	kill_timer.connect("timeout",self,"queue_free")	

func _physics_process(delta):
	self.translation += direction * delta * speed
	
func _on_body_entered(body):
	emit_signal("hitted_something", body)
	kill_timer.disconnect("timeout",self,"queue_free")
