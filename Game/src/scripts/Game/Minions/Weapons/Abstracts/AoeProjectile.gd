tool
extends Node
class_name AoeProjectile

export(Gradient) var color_gradient
export(Color) var area_color

signal hitted_something(body)

var ExplosionTween = Tween.new()
var fade_gradient = Gradient.new()
var cilinder_height = 0.2

var target_point : Vector3
var area_radius : float
var delay : float
var animation_time : float
var fade_time : float
var ease_explotion = Tween.EASE_IN

onready var sphere_radius = Vector3(area_radius,area_radius,area_radius)

func _ready():
	add_child(ExplosionTween)
	
	fade_gradient.set_color(0,color_gradient.interpolate(1))
	fade_gradient.set_color(1,Color.transparent)
	
	#warning-ignore: return_value_discarded
	var target_area_material = $TargetArea.get_surface_material(0)
	$TargetArea.scale = Vector3(area_radius,cilinder_height,area_radius)
	target_area_material.albedo_color = area_color
	target_area_material.albedo_color.a *= .5
	target_area_material.emission = area_color
	$ExplosionArea.connect("body_entered", self, "_on_body_entered")
	
	$ExplosionArea/CollisionShape.shape.set_radius(area_radius)
	
	attack()

func _on_body_entered(body):
	emit_signal("hitted_something", body)

func attack():
	self.global_transform.origin = target_point #OJO ESTOY SETEANDO CON GLOBAL
	$TargetArea.visible = true
	yield(get_tree().create_timer(delay), "timeout")
	$ExplosionArea/CollisionShape.disabled = false
	yield(explosion_animation(),"completed")
	if not Engine.editor_hint:
		queue_free()

func explosion_animation():
	var mesh_area = $ExplosionArea/MeshInstance
	mesh_area.visible = true
	ExplosionTween.interpolate_property(
		mesh_area,"scale",
		Vector3.ZERO,
		sphere_radius,
		animation_time,
		Tween.TRANS_LINEAR,
		ease_explotion 
	)
	ExplosionTween.interpolate_method(self,"set_mesh_color",0,1,animation_time,
		Tween.TRANS_LINEAR, ease_explotion
	)
	ExplosionTween.start()
	yield(ExplosionTween,"tween_all_completed")
	ExplosionTween.remove_all()
	ExplosionTween.interpolate_method(self,"set_mesh_fade",0,1,fade_time)
	ExplosionTween.start()
	yield(ExplosionTween,"tween_completed")

func set_mesh_color(t):
	var material = $ExplosionArea/MeshInstance.get_surface_material(0)
	material.emission = color_gradient.interpolate(t)
	material.albedo_color = color_gradient.interpolate(t)

func set_mesh_fade(t):
	var material = $ExplosionArea/MeshInstance.get_surface_material(0)
	material.albedo_color = fade_gradient.interpolate(t)
