tool
extends Node
class_name AoeAttack

export(PackedScene) var weapon_scene = null setget set_weapon_node
export(PackedScene) var projectile_scene

func attack(target_point):
	$AnimationPlayer.play("CastSpell")
	var projectile_instance = projectile_scene.instance()
	projectile_instance.get_node("Projectile").target_point = target_point
	projectile_instance.set_as_toplevel(true)
	add_child(projectile_instance)

func set_weapon_node(value:PackedScene):
	weapon_scene = value
	if value != null:
		if has_node("Object"):
			get_node("Object").free()
		var value_node = value.instance()
		add_child(value_node)
		value_node.name = "Object"


func _unhandled_input(event): #Señal del evento de ingreso a base
	if event is InputEventKey and event.pressed and event.scancode == KEY_ENTER:
		attack(Vector3.ZERO)
