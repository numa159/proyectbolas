tool
extends Node
class_name MeleeAttack

export(PackedScene) var weapon_scene = null setget set_weapon_node

signal hitted_something(body)

func _ready():
	#warning-ignore: return_value_discarded
	$HitArea.connect("body_entered", self, "_on_body_entered")
	
func _on_body_entered(body):
	emit_signal("hitted_something", body)

func attack():
	pass

#[NOTE](@lordmanuel): @dapert, esto esta repetido en RangedAttack
func set_weapon_node(value:PackedScene):
	weapon_scene = value
	if value != null:
		if has_node("Object"):
			get_node("Object").queue_free()
		var value_node = value.instance()
		add_child(value_node)
		value_node.name = "Object"
