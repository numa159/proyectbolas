tool
extends Node
class_name RangedAttack

export(PackedScene) var weapon_scene = null setget set_weapon_node
export(PackedScene) var projectile

func attack():
	var projectile_node = projectile.instance()
	projectile_node.set_as_toplevel(true)
	add_child(projectile_node)

#[NOTE](@lordmanuel): @dapert, esto esta repetido en MeleeAttack
func set_weapon_node(value:PackedScene):
	weapon_scene = value
	if value != null:
		if has_node("Object"):
			get_node("Object").queue_free()
		var value_node = value.instance()
		add_child(value_node)
		value_node.name = "Object"
