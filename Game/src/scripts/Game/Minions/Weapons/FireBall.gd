tool
extends AoeProjectile


func _init():
	area_radius = 5
	delay = 1
	animation_time = 0.2
	fade_time = 0.5
	
	target_point = Vector3.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
