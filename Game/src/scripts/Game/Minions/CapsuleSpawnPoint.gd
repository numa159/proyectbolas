extends Spatial
var player = null
var parent = null

func set_capsule_parent(node):
	parent = node

func set_player(set_player):
	player = set_player

func spawn_capsules(quantity_list: MinionSpawnList):
	if not player:
		printerr("No player found for capsule spawn")
		return
	
	var d = Data.MINION.radius * Data.MINION.scale * 2 + 0.6
	var vx = Vector2(d,0)
	var vy = Vector2(d,0).rotated(PI/3)
	var positions = HexGrid.generate_relative_positions(quantity_list.total())
	
	var list_of_capsules_to_spawn = quantity_list.get_as_array()
	if list_of_capsules_to_spawn.size() == 0:
		return
	assert(len(list_of_capsules_to_spawn) == len(positions)) 
	
	for index_pos in len(positions):
		var capsule_to_spawn = list_of_capsules_to_spawn[index_pos]
		var pos = positions[index_pos]
		var offset = pos.x * vx + pos.y * vy
		var capsule_instance = Data.MINION.list[capsule_to_spawn].instance()
		
		capsule_instance.get_child(0).translation = \
			global_transform.origin + Vector3(offset.x, 0, offset.y)
		
		capsule_instance.get_child(0).follow_player(player)
		capsule_instance.get_child(0).owned_by = player
		
		parent.add_child(capsule_instance)

