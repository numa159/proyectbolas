tool
extends KinematicBody
class_name CapsuleMinion

export(PackedScene) var right_hand = null
export(PackedScene) var left_hand = null
export(float) var jump_time
export(float) var jump_delay

onready var jump_distance = Data.MINION.jump_distance
onready var search_enemy_timer = Timer.new()
onready var mesh = $MeshParent

var owned_by : PlayerNode
var attack_speed : float
var to_player : Vector3
var origin : Vector3

var waiting_time_search = 2
var velocity := Vector3(0,0,0)
var floored = false;
var jumping = false;
var following_player = null
var target_point = null
var grid_pos = null
var nearest_minion_enemy = null

func _ready():
	set_meta("pushable",true)
	setup()

func _physics_process(delta):
	if Engine.editor_hint:
		return
	apply_gravity_and_move(delta)
	floored = is_on_floor()
	if floored: velocity = Vector3.ZERO;
	if self.velocity.y <= 0:
		self.get_node("CollisionShape").disabled = false
	
	# [TODO] Si el piso tiene desnivel no va
	origin = self.translation - Vector3.UP * self.translation 

	if not following_player:
		return
	to_player = self.following_player.translation - self.translation
	look_at_player()

func follow_player(player):
	following_player = player.body

func disable_collision(): #Se va a llamar cuando la están empujando
	$CollisionShape.disabled = true 

func setup():
	if not Engine.editor_hint:
		var player_id = owned_by.id
		set_layer(player_id)
		set_color()
	entity_data()
	add_ons()
	setup_timer()

func entity_data():
	var radius = Data.MINION.radius;
	$CollisionShape.shape.radius = radius;
	$MeshParent/MeshInstance.mesh.radius = radius;
	$RHand.translation.x = -1.3 * radius;
	$LHand.translation.x = 1.3 * radius;
	$RHand.translation.z = 0.4 * radius;
	$LHand.translation.z = 0.4 * radius;
	$Hat.translation.y = 1 * radius;
	var vector_scale = Vector3(1,1,1)*Data.MINION.scale;
	self.scale = vector_scale

func add_ons():
	if right_hand != null:
		var instance = right_hand.instance()
		$RHand.add_child(instance)

func look_at_player():
	var v2_to_player = Vector2(to_player.z,to_player.x)

	#[NOTE](@lordmanuel): rotation xq angle devuelve radianes.
	rotation.y = v2_to_player.angle()

func apply_gravity_and_move(delta):
	velocity += Vector3(0,-9.8*delta,0)
	#warning-ignore: return_value_discarded
	move_and_slide(velocity, Vector3.UP)

func set_layer(id):
	set_collision_layer_bit((id*2)+2,true)
	
	$AggroArea.set_collision_mask_bit((id*2)+1,false) #Deactive the friend player mask

func set_color():
	var mat = get_node("MeshParent/MeshInstance").get_surface_material(0);
	var player_color = owned_by.color
	mat.emission_enabled = true;
	mat.emission = player_color;

func setup_timer():
	add_child(search_enemy_timer)
	search_enemy_timer.connect(
		"timeout", 
		self, 
		"_on_search_timer_timeout"
	);
	search_enemy_timer.wait_time = waiting_time_search
	search_enemy_timer.autostart = true
	search_enemy_timer.start()

func _on_search_timer_timeout():
	search_enemy()

func search_enemy():
	if Engine.editor_hint:
		return
	nearest_minion_enemy = MinionHiveMind.get_nearest_enemy(self)
