extends "res://src/classes/StateMachine.gd"

var body : CapsuleMinion setget set_body 

func _ready():
	set_body(get_parent())
	states_map = {
		"actioning": $ActionState,
		"jumping": $JumpingState,
		"falling": $FallingState,
		"recovering": $RecoveringState,
	}
	set_active(true)

func _physics_process(delta):
	if body.velocity.y <= 0  and \
		current_state != $FallingState and \
		not body.floored and \
		not body.jumping:
		_change_state("falling")
		return;
	current_state.update(delta)

func set_body(new_body):
	print("Se seteo!!!", new_body)
	body = new_body
	for state in get_children():
		state.body = new_body
