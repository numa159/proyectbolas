extends Spatial

const COLORS = [
	Color.green,
	Color.red,
	Color.cyan,
	Color.yellow
];

var player_scene := preload("res://scenes/Game/Player/Player.tscn");
sync var players_done := [];
var peer :NetworkedMultiplayerPeer;
var peer_connected := false

signal all_players_ready();

func _process(_delta):
	if not peer:
		peer = get_tree().network_peer;
	if peer and not peer_connected:
		peer_connected = true;
		if peer.connect("connection_succeeded", self, "spawn_players") != OK:
			printerr("Error connecting connection_succeeded");

func _ready():
	GameData.players_node = self;
	if Lobby.connect(
		"player_joined", 
		self, 
		"spawn_player"
	) != OK:
		printerr("Error connecting network_peer_connected");

func spawn_players():
	for id in Lobby.player_info.keys():
		spawn_player(id);

func spawn_player(id: int):
	if has_node(String(id)):
		return;
	var player = player_scene.instance();
	player.set_network_master(id);
	player.name = String(id);
	add_child(player);

	if get_children().size() == Lobby.player_count:
		rpc_id(1, "done_spawning_players");

remotesync func done_spawning_players():
	var who = get_tree().get_rpc_sender_id();
	players_done.append(who);
	rset('players_done', players_done);

	if players_done.size() == Lobby.player_count:
		rpc("all_players_ready");

func _player_connected(id:int):
	spawn_player(id);

remotesync func all_players_ready(): # Ya estan todos en partida!
	for i in range(Debug.print(players_done).size()):
		var player = get_node(String(players_done[i]));
		player.id = i;
		player.set_layer();
		player.health = load("res://assets/resources/healths/Player%d.tres"%i);
		if player.is_network_master():
			GameData.player_health = player.health;
		player.health.reset();
		player.color = COLORS[i];
	emit_signal("all_players_ready");
