extends Control

func update_health(current_health):
	$TextureProgress.value = current_health;
	
func set_health():
	#warning-ignore: return_value_discarded
	GameData.player_health.connect("health_changed", self, "update_health")
		
func _ready():
	#warning-ignore: return_value_discarded
	GameData.connect("health_set", self, "set_health");
