extends Node

onready var login_container = $Centerer;
onready var debug_container = $VBoxContainer;
onready var debug_panel = $VBoxContainer/DebugPanel;

func _ready():
	#warning-ignore: return_value_discarded
	Matchmaking.connect("joined", self, "_on_join_lobby");

func _on_join_lobby():
	debug_container.show();
	login_container.hide();
