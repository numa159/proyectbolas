tool
extends Control

var username := "";

func _ready()->void:
	#warning-ignore: return_value_discarded
	$Username.connect("text_changed", self, "_set_name");
	#warning-ignore: return_value_discarded
	$Username.connect("text_entered", self, "_focus_button");
	#warning-ignore: return_value_discarded
	$FindMatch.connect("pressed", self, "_try_match");
	#warning-ignore: return_value_discarded
	$Test.connect("pressed", self, "_go_test");

func _set_name(new_text: String)->void:
	if not new_text: 
		$FindMatch.disabled = true;
	else:
		$FindMatch.disabled = false;
	username = new_text;
	Lobby.my_info.username = username;
	Connection.username = username

func _focus_button(new_text: String)->void:
	if not new_text: return;
	$FindMatch.grab_focus()

func _try_match()->void:
	Connection.find_match();

func _go_test():
	#[TODO]: Remove off testing environment
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(42069, 1);
	get_tree().network_peer = peer;
	get_node("/root/Main/Interface")._on_join_lobby();
	Lobby._add_self();
