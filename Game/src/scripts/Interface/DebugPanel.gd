extends Panel

func write_message(message:String)->void:
	var label := Label.new();
	label.text = message;
	label.align = Label.ALIGN_CENTER;
	$VB.add_child(label);

func clear()->void:
	for label in $VB.get_children():
		label.queue_free();
