tool
extends Node

signal points_updated;
signal health_set;

var players_node : Node;
var points := PoolIntArray([60,0,0,0]);
var player_health : PlayerHealth setget set_player_health;

func set_player_health(value):
	player_health = value
	emit_signal("health_set")

func add_points(amt:int, player:PlayerNode):
	points[player.id] += amt;
	emit_signal("points_updated");
