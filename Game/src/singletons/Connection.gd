extends Node

const SERVER_KEY = "pitball"
const SERVER_IP = "190.16.250.122"
const SERVER_PORT = 7350

const MIN_PLAYERS = 3
const MAX_PLAYERS = 4

const CLIENT_VERSION = "dev"

var nakama_client: NakamaClient
var nakama_session: NakamaSession
var nakama_socket: NakamaSocket

var username := ""


func find_match():
	if not username:
		return
	connect_to_nakama()


func connect_to_nakama():
	# Connect to a local Nakama instance using all the default settings.
	nakama_client = Nakama.create_client(
		SERVER_KEY, SERVER_IP, SERVER_PORT
	)

	# Login to Nakama using "device authentication".
	var device_id = OS.get_unique_id()
	nakama_session = yield(
		nakama_client.authenticate_device_async(device_id, username),
		"completed"
	)
	if nakama_session.is_exception():
		print("Unable to connect to Nakama")
		get_tree().quit()

	# Open a realtime socket to Nakama.
	nakama_socket = Nakama.create_socket_from(nakama_client)
	yield(nakama_socket.connect_async(nakama_session), "completed")
	print("Connected to Nakama!")
	join_an_online_match()


func join_an_online_match():
	# We can configure OnlineMatch before using it:
	OnlineMatch.min_players = MIN_PLAYERS
	OnlineMatch.max_players = MAX_PLAYERS
	OnlineMatch.client_version = CLIENT_VERSION
	OnlineMatch.ice_servers = [
		{"urls": ["stun:stun.l.google.com:19302"]}
	]
	OnlineMatch.use_network_relay = OnlineMatch.NetworkRelay.DISABLED
	#OnlineMatch.NetworkRelay.AUTO should be better in production
	#[TODO]: configure STUN/TURN servers

	var connections = {
		"error": "_on_error",
		"disconnected": "_on_disconnected",
		"matchmaker_matched": "_on_matchmaker_matched",
		"player_joined": "_on_player_joined",
		"player_left": "_on_player_left",
		"player_status_changed": "_on_player_status_changed",
		"match_ready": "_on_match_ready",
		"match_not_ready": "_on_match_not_ready",
	}

	for signal_name in connections.keys():
		#warning-ignore: return_value_discarded
		OnlineMatch.connect(
			signal_name, Matchmaking, connections[signal_name]
		)

	# Join the matchmaking queue.
	OnlineMatch.start_matchmaking(nakama_socket)
	print("Joined the matchmaking queue...")
