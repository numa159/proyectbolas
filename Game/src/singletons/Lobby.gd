extends Node

var player_info := {};
var my_info = {username = ""};
var player_count := 1;

signal player_joined(id);

func _ready():
	var tree = get_tree();
	if Matchmaking.connect("joined", self, "_add_self") != OK or \
		tree.connect("network_peer_connected", self, "_joined") != OK or \
		tree.connect("network_peer_disconnected", self, "_left") != OK or \
		tree.connect("connected_to_server", self, "_connected_ok") != OK or \
		tree.connect("connection_failed", self, "_connected_fail") != OK or \
		tree.connect("server_disconnected", self, "_server_disconnected") != OK:
		printerr("Error conecting signal on Lobby");

func _add_self():
	var my_id = 1;
	if get_tree().has_network_peer():
		my_id = get_tree().get_network_unique_id()
	player_info[my_id] = my_info
	emit_signal("player_joined", my_id);
	
func _joined(id: int) -> void:
	rpc_id(id, "register_player", my_info);

func _left(id:int):
	if not player_info.erase(id):
		printerr("Player not found with id: %d" % id);

func _connected_ok():
	printerr("Connection failed")
	pass;

func _connected_fail():
	printerr("Connection failed")
	pass;

func _server_disconnected():
	printerr("Server disconnected")
	pass;

remote func register_player(info):
	var id = get_tree().get_rpc_sender_id();
	player_info[id] = info;
	emit_signal("player_joined", id);
