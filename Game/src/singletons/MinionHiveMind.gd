extends Node

var radius = Data.MINION.radius * 2 * Data.MINION.scale 
var grid_map : Dictionary

const FILE_PATH = "res://assets/resources/positions_cache.tres";

func _ready():
	var resource = load(FILE_PATH)
	if resource.data:
		grid_map = resource.data;
	else:
		var map_radius = Data.MAP.radius;
		generate_grid_map(
			generate_positions(
				Vector2(map_radius, map_radius)*2.0, 
				Vector3(-map_radius,0,-map_radius)
			)
		)
		resource.data = grid_map;
		print(resource.data);
		if ResourceSaver.save(FILE_PATH, resource) != OK:
			printerr("Error saving GridMap Resource");
	
func generate_positions(dimension: Vector2, corner_map: Vector3):
	var positions2D = \
		PoissonDiscSampling.generate_points(radius, dimension/2.0);
	var positions3D = PoolVector3Array();
	var corners = [
		Vector3.ZERO,
		corner_map*Vector3(1,0,0),
		corner_map*Vector3(0,0,1),
		corner_map,
	];
	for pos2D in positions2D:
		for corner in corners:
			positions3D.append(corner + Vector3(pos2D.x,0,pos2D.y))
	return positions3D

func get_nearest(position:Vector3):
	var position_in_grid = map_point_to_grid(position)
	if grid_map.has(position_in_grid) and not is_occupied(position_in_grid):
		return grid_map[position_in_grid][0]
	var length = INF
	var result = null
	var resultant_map = null
	for i in range(5):
		for j in range(5):
			var mgp = Vector2(i-2,j-2)+position_in_grid
			if mgp.is_equal_approx(position_in_grid) or \
				not grid_map.has(mgp) or \
				is_occupied(mgp):
					continue
			var p3d = grid_map[mgp][0]
			var dist = p3d.distance_squared_to(position)
			resultant_map = mgp if dist < length else resultant_map
			result = p3d if dist < length else result
			length = min(length, dist)
	if result == null or resultant_map == null:
		return null
	return result


func generate_grid_map(positions: PoolVector3Array):
	print("PoissonGridMap generated")
	for pos in positions:
		grid_map[map_point_to_grid(pos)] = [pos, null]

func map_point_to_grid(position: Vector3):
	return (Vector2(position.x, position.z)/(2*radius)).floor()

func occupy(posi:Vector2, node: CapsuleMinion):
	grid_map[posi][1] = node

func unoccupy(posi:Vector2):
	grid_map[posi][1] = null

func is_occupied(posi:Vector2):
	return not not grid_map[posi][1]

func get_nearest_enemy(minion: CapsuleMinion):
	var position = minion.grid_pos
	var id = minion.owned_by.id
	minion.get_node("AggroArea") #Chequear si hay un enemigo dentro de estos limites

	#[TODO] Calculate nearest enemy and return it or null.
	return null
