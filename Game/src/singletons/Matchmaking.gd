extends Node

signal joined();

func _on_error(message: String)->void:
	printerr("Error: %s" % message);

func _on_disconnected()->void:
	print("Disconnected from match");

func _on_matchmaker_matched(players: Dictionary)->void:
	print("Joined via matchmaker!");
	for player in players.values():
		print("Player found: %s" % player.username)
	Lobby.player_count = players.values().size();

func _on_player_left(player: OnlineMatch.Player)->void:
	print("Player left: %s" % player.username);

func _on_player_joined(player: OnlineMatch.Player)->void:
	print("Player joined: %s" % player.username);

func _on_player_status_changed(player: OnlineMatch.Player, status)->void:
	print("Player status changed: %s -> %s" % [player.username, status])
	if player.peer_id != get_tree().get_network_unique_id() && status == OnlineMatch.PlayerStatus.CONNECTED:
		rpc_id(player.peer_id, "receive_message", "Hi! We're connected now :-)")

remote func receive_message(message: String) -> void:
	print("Message from %s: %s" % [get_tree().get_rpc_sender_id(), message])

func _on_match_not_ready() -> void:
	print("The match isn't ready to start")
 
func _on_match_ready(players: Dictionary) -> void:
	print("The match is ready to start! Here are players:")
	for player in players.values():
		print ("- %s" % player.username)
 
	OnlineMatch.start_playing()
	if get_tree().is_network_server():
		rpc("start_game")
 
remotesync func start_game() -> void:
	emit_signal("joined");
