tool
extends Node

const CAMERA = {
	"height": 20,
	"distance": 40,
	"acceleration": 200
};

const BALL = {
	"radius": 1,
};

const MINION = {
	"player_range": 30, #si está más de esta distancia el player las capsulitas dejan de atacar y lo siguen
	"scale": 0.4,
	"radius": 0.6,
	"jump_distance": 0.4 * 0.6 * 10 * 1.5,
	"list": {
		"minion": preload("res://scenes/Game/Minions/DefaultMinion.tscn"),
		"ranged_minion": preload("res://scenes/Game/Minions/RangedMinion.tscn"),
		"mage_minion": preload("res://scenes/Game/Minions/MageMinion.tscn") 
	},
};

const MAP = {
	"radius": 100,
	"pit_depth": 5,
	"pit_span": 20,
	"wall_height": 100,
};
