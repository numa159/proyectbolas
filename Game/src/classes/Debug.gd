extends Reference
class_name Debug

static func print(something):
	if OS.is_debug_build():
		print("Debug: ", something);
	return something;

