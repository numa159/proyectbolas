extends Node
class_name TweenAnimator

const TRANS = Tween.TRANS_CIRC
const EASE = Tween.EASE_IN_OUT

static func animate(
	tween: Tween, 
	time: float, 
	list: Dictionary, 
	on_node : Node,
	trans := TRANS, 
	ease_func := EASE 
):
	for key in list.keys():
		if not tween.interpolate_property(
			on_node, 
			key, 
			null, 
			list[key], 
			time, 
			trans, 
			ease_func
		):
			printerr("Error setting up tween animation");
