tool
extends Resource
class_name PoissonPoints

export(PoolVector2Array) var points : PoolVector2Array setget set_points;

func generate(size: Vector2, radius: float):
	points = PoissonDiscSampling.generate_points(radius, size);

func set_points(n_points:PoolVector2Array):
	points = n_points;
	emit_changed();
