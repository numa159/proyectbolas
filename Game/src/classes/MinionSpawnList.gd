extends Reference
class_name MinionSpawnList

var list = {}

func _init(initial_list={}):
	if validate_input_list(initial_list):
		list = initial_list
	else:
		printerr("Invalid MinionSpawnList")

func validate_input_list(input_list):
	for capsule_key in input_list.keys():
		if not (capsule_key in Data.MINION.list.keys()):
			return false
	for capsule_quantity_value in input_list.values():
		if not typeof(capsule_quantity_value) == TYPE_INT and capsule_quantity_value >= 0:
			return false
	return true

func add_capsule_spawn(adding_list):
	if not validate_input_list(adding_list):
		printerr("Invalid new MinionList")
		return
	for adding_key in adding_list.keys():
		list[adding_key] = (0 if not list.has(adding_key) else list[adding_key])+adding_list[adding_key]

func get_as_array():
	var list_result = []
	for dict_key in list.keys():
		for i in list[dict_key]:
			list_result.append(dict_key)
	return list_result

func total():
	var total = 0
	for value in list.values():
		total += value
	return total
