extends Node
class_name State

#warning-ignore: unused_signal
signal finished(next_state_name)

func enter():
	return

func exit():
	return

#warning-ignore: unused_argument
func handle_input(event):
	return

#warning-ignore: unused_argument
func update(delta):
	return
