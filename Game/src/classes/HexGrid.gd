extends Node
class_name HexGrid

const STEPS = [Vector2(-1,1), Vector2(-1,0), Vector2(0,-1), Vector2(1,-1),
Vector2(1,0), Vector2(0,1)]

static func generate_relative_positions(total_number):
	var positions = []
	var curr_position = Vector2(0,0)
	for i in range(total_number - 1):
		positions.append(curr_position)
		curr_position = gen_next_pos(i, curr_position)
	positions.append(curr_position)
	return positions

static func quant_until_n(n):
	var total_quant = 0
	for i in range(n):
		total_quant += 6 * (i + 1)
	return total_quant

static func generate_n(from_i):
	var n = 0
	while from_i > quant_until_n(n):
		n += 1
	return n

static func gen_next_pos(iteration: int, current:Vector2): 
	if iteration == 0:
		return current + Vector2(1,0)
	
	var current_n = generate_n(iteration)
	if iteration == quant_until_n(current_n): #Si es el último step del ciclo
		return current + Vector2(1, 1)
	
	var step = floor((iteration + (current_n - 1) - quant_until_n(current_n - 1)) / current_n) 
	return current + STEPS[step - 1]
