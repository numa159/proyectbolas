extends Node
class_name NumberRenderer

const NUMBERS = preload("res://assets/textures/DisplayAnimations/Numbers.png");
const DIGIT_SIZE = 5;

var numbers := [];

func _init():
	setup_numbers();

func setup_numbers():
	var digits = NUMBERS.get_data();
	for i in range(10):
		numbers.push_back(
			digits.get_rect(
				Rect2(Vector2(DIGIT_SIZE * i,0), Vector2(1,1)*DIGIT_SIZE)
			)
		);

func create_number_image(number: int, min_digits := 1 )->Image:
	var str_num = "%<num>d".replace("<num>", String(min_digits)) % number;
	var total_digits = str_num.length();
	var image = Image.new();
	image.create(
		(DIGIT_SIZE-1)*min_digits+1,
		DIGIT_SIZE+1,
		false,
		NUMBERS.get_data().get_format()
	);
	image.lock();
	image.fill(Color.black)
	var i = 0;
	for c in str_num:
		var digit_idx = min_digits - total_digits + i;
		draw_number(image, c, digit_idx);
		i+=1;
	image.unlock();
	return image

func draw_number(image: Image, c: String, index: int)->void:
	image.blit_rect(
		numbers[int(c)],
		Rect2(Vector2.ZERO, Vector2(DIGIT_SIZE - 1, DIGIT_SIZE)),
		Vector2(index*(DIGIT_SIZE-1),1)
	);
