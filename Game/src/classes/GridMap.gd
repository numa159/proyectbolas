tool
extends Resource
class_name PoissonGridMap

export(Dictionary) var data : Dictionary setget set_data;

func set_data(n_data: Dictionary):
	data = n_data;
	emit_changed();
